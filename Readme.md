## Python Language Workshop - Maze
### Epitech - 2019

Proyecto realizado durante el workshop de Epictech en la UNLP sobre
programación en Python 3.

##### Consigna:
Se requirió implementar un juego de laberinto donde el jugador tenga que
llegar hasta un punto definido como la salida. Posteriormente se solicitó el
diseño de una inteligencia artificial que pueda resolver el laberinto por sus
propios métodos.

#### Características Implementadas:
- El juego permite imprimir en pantalla un laberinto y la posición de los
jugadores; Adquiriendo entrada desde teclado, permite el movimiento por el
mismo.

- Se incorporó la posibilidad de cargar el laberinto desde un archivo de texto,
permitiendo así hacer uso de herramientas de generación de laberintos
disponibles en la web.

- Se incorporó un contador de movimientos y condiciones de victoria.

- Se desarrolló una IA que recorre el laberinto por backtracking, esto es que
recorre secuencialmente un camino mientras haya celdas sin visitar, y en caso
de quedarse encerrada, vuelve por donde vino hasta una nueva bifurcación con
celdas que no haya visitado.

- Se implementaron los dos jugadores al mismo tiempo, lo que permite jugar contra
la IA.

- La decision de qué camino tomar frente a varias posibilidades se realiza aleatoriamente
por la IA.

- El juego carga un laberinto aleatorio del directorio "mazes" al arrancar.

- Implementada deteccion de plataforma para llamadas especificas de sistema. Ahora soporta Windows y Linux.
