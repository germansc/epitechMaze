#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ---------------------
#
# Germán Sc. | 2019
# -----------------
import sys, os, platform, random

""" Brief function to get keypresses on Linux or Windows """
def getch():
	if platform.system() is 'Linux':
		import termios, tty

		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)

		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
			return ch

	elif platform.system() is 'Windows':
		import msvcrt
		return msvcrt.getch().decode('ASCII')

	else:
		print("OS not supported.")


""" CLASS: PLAYER
ATTRIBUTES:
  xpos : Position of the player on the X axis.
  ypos : Position of the player on the Y axis.
  movements : Number of moves taken.
  char : Character for representation on the maze.
  history : Copy of the maze used to register visited cells.

METHODS:
  move(x,y) : Sets the x and y attributes of the instance.
  checkPosition(x,y) : Returns boolean indicating if the instance is located on
                       the given x and y values.
  loadMap(newMap) : Generates a copy of the given maze for self-reference.
  setHistory(dir) : Stores on the current coordinates of the history the values
                    of dir. It's used to indicate the cell from which we came.
"""
class Player:
	def __init__(self, char):
		# Initialize instance attributes.
		self.xpos = -1
		self.ypos = -1
		self.movements = 0
		self.char = char
		self.history = []

	def move(self, x, y):
		self.xpos = x
		self.ypos = y

	def checkPosition(self, x, y):
		return ((x == self.xpos) and (y == self.ypos))

	def loadMap(self, newMap):
		self.history.clear()
		for index in range(len(newMap)):
			temp = []
			temp = list(newMap[index])
			self.history.append(temp)

	def setHistory(self, dir):
		if self.history[self.ypos][self.xpos] == 0:
			self.history[self.ypos][self.xpos] = dir


""" CLASS: MAZE
ATTRIBUTES:
  won : Indicates if the win condition was reached.
  running : Indicates if the game is running.
  map  : List of lists with the loaded maze.
  pawn : Player instance.
  ai   : AI Player instance.

METHODS:
  loadMaze(file) : Loads a maze from a textfile. Useful with online maze generators.
  keyboardMove() : Waits and process the player movement.
  aiMove() : Process the AI movement.
  tryMovePiece(piece, x, y) : Attempts to move the given piece to the given coordinates.
                              Returns True if the movement was valid. False otherwise.
  display() : Prints the maze and players on the terminal.
  printHelp() : Prints the header with the objective, controls and scores.
"""
class Maze:
	# Attributes.
	won = False
	map = []
	pawn = Player('0')
	ai = Player('A')
	running = True

	# Constructor - FIXME: Doesn't do anything useful currently.
	def __init__(self, rowNum, colNum):
		# Go through the rows list:
		for rindex in list(range(rowNum)):

			# Generate a columns list (a single row).
			temp = []
			for cindex in list(range(colNum)):
				temp.append(rindex * colNum + cindex)

			# Append the generated row to the rows list.
			self.map.append(temp)

	def randomFill(self):
		# TBD - Some day.
		pass

	def loadMaze(self, file):
		# Clear the current map.
		self.map.clear()
		rindex = 0

		# Open the file, go through each line.
		input = open(file, 'r')
		for line in input:
			temp = []
			cindex = 0

			# For each character, evaluate the tile value.
			for x in line:
				if x.upper() == "E":
					temp.append(2)
				elif x.upper() == "C":
					temp.append(3)
				elif x.upper() == " ":
					temp.append(0)
				else:
					# Pared.
					temp.append(1)

				# Increment the columns index.
				cindex += 1

			# Append the generated row to the map.
			self.map.append(temp)
			rindex += 1

		# Pass as reference for the players history.
		self.pawn.loadMap(self.map)
		self.ai.loadMap(self.map)

		# Locate the players. -- FIXME: This implementation should be reviewed.
		for rindex in range(len(self.map)):
			for cindex in range(len(self.map[0])):
				if self.map[rindex][cindex] == 2:
					self.pawn.move(cindex, rindex)
					self.pawn.setHistory("U")
					self.ai.move(cindex, rindex)
					self.ai.setHistory("U")
					return

	def keyboardMove(self):
		# Wait for a keypress.
		key = getch()

		# Evaluate the key.
		if key == 'w':
			res = self.tryMovePiece(self.pawn, self.pawn.xpos, self.pawn.ypos - 1)
			if res: self.pawn.setHistory("D")
		elif key == 's':
			res = self.tryMovePiece(self.pawn, self.pawn.xpos, self.pawn.ypos + 1)
			if res: self.pawn.setHistory("U")
		elif key == 'a':
			res = self.tryMovePiece(self.pawn, self.pawn.xpos - 1, self.pawn.ypos)
			if res: self.pawn.setHistory("R")
		elif key == 'd':
			res = self.tryMovePiece(self.pawn, self.pawn.xpos + 1, self.pawn.ypos)
			if res: self.pawn.setHistory("L")
		else:
			self.running = False

	def aiMove(self):
		result = False

		# List of posible movements as three-element-tupples: (destinationX, destinationY, originCell)
		# {UP, DOWN, LEFT, RIGHT}
		moveList = [(self.ai.xpos, self.ai.ypos -1, "D" ), (self.ai.xpos, self.ai.ypos + 1, "U"),
					(self.ai.xpos - 1, self.ai.ypos, "R"), (self.ai.xpos + 1, self.ai.ypos, "L")]

		# While there are moves to check:
		while len(moveList):
			# Pick a random direction:
			choice = random.choice(moveList)
			moveList.remove(choice)

			# If that direction was NOT visited and NOT a wall:
			if self.ai.history[choice[1]][choice[0]] in [0, 3]:

				# Try to go in that direction if success, leave the loop.
				result = self.tryMovePiece(self.ai, choice[0], choice[1])
				if result:
					self.ai.setHistory(choice[2])
					break

		# If it wasn't able to move it's trapped, go back.
		if not result:
			if self.ai.history[self.ai.ypos][self.ai.xpos] is "U":
				self.tryMovePiece(self.ai, self.ai.xpos, self.ai.ypos - 1)

			elif self.ai.history[self.ai.ypos][self.ai.xpos] is "D":
				self.tryMovePiece(self.ai, self.ai.xpos, self.ai.ypos + 1)

			elif self.ai.history[self.ai.ypos][self.ai.xpos] is "L":
				self.tryMovePiece(self.ai, self.ai.xpos - 1, self.ai.ypos)

			elif self.ai.history[self.ai.ypos][self.ai.xpos] is "R":
				self.tryMovePiece(self.ai, self.ai.xpos + 1, self.ai.ypos)


	def tryMovePiece(self, piece, x, y):
		# Check if the move goes out of bounds.
		if ((x not in range(len(self.map[0]))) or (y not in range(len(self.map)))):
			return False

		# Check if the movement ends in a wall.
		if self.map[y][x] != 1:
			piece.move(x,y)
		else:
			return False

		# Check if the exit is reached.
		if self.map[y][x] == 3:
			# Check if player won.
			if (piece == self.pawn):
				self.won = True

			# End the game.
			self.running = False

		# Movements++.
		piece.movements += 1

		# Valid move, return True.
		return True

	def display(self):
		# Clear the screen
		if platform.system() is 'Linux':
			os.system('clear')
		elif platform.system() is 'Windows':
			os.system('cls')
		else:
			print("OS not supported.")

		# Print the header.
		self.printHelp()

		# Go through the rows list:
		for rindex in list(range(len(self.map))):

			# Go throught the columns list:
			for cindex in list(range(len(self.map[rindex]))):

				# Check the value of the tile.
				if self.map[rindex][cindex] == 1:
					print("X", end = '')

				# Check if hallway.
				elif self.map[rindex][cindex] == 0:
					# Any non-wall tile will check if any player is located there.
					if self.pawn.checkPosition(cindex, rindex):
						print(self.pawn.char, end = '')
					elif self.ai.checkPosition(cindex, rindex):
						print(self.ai.char, end = '')
					else:
						# Commented code was useful for debugging, prints AI history.
						#if self.ai.history[rindex][cindex] not in [0, 1]:
						#	print(self.ai.history[rindex][cindex], end = '')
						#else:
						#	print(" ", end = '')
						print(" ", end = '')

				# Check if Entrance.
				elif self.map[rindex][cindex] == 2:
					if self.pawn.checkPosition(cindex, rindex):
						print(self.pawn.char, end = '')
					elif self.ai.checkPosition(cindex, rindex):
						print(self.ai.char, end = '')
					else:
						print("E", end = '')

				# Check if Exit.
				elif self.map[rindex][cindex] == 3:
					if self.pawn.checkPosition(cindex, rindex):
						print(self.pawn.char, end = '')
					elif self.ai.checkPosition(cindex, rindex):
						print(self.ai.char, end = '')
					else:
						print("C", end = '')

			# Move to next line.
			print("")

	def printHelp(self):
		print("MAZEMASTER 2000\n\nGET TO THE C!\n[W, A, S, D] for movement\nAnything else to quit\n")
		print("Your moves: " + str(self.pawn.movements) + " | AI moves: " + str(self.ai.movements) + "\n")


""" MAIN GAME LOOP """
if __name__ == "__main__":

	# Generate a new Maze object.
	maze = Maze(10,10)

	# Load random maze from the "mazes" folder.
	maze.loadMaze("mazes/" + random.choice(os.listdir("mazes")))
	maze.display()

	# Game loop.
	while maze.running:

		# Player movement.
		maze.keyboardMove()

		# AI Movement.
		maze.aiMove()

		# Display new data.
		maze.display()

	# Game not running, check if player won.
	if maze.won:
		print("\nYOU WIN!\nTotal moves: " + str(maze.pawn.movements))

	# Win or Lose, the game is over.
	print("\nGAME OVER\n")
